<?php

$server = $_SERVER['HTTP_HOST'];
$localhost = "localhost";
$sessao = md5("QueroMorrer");
$chave = "6c02e56opujoioijusc78b2ititiu02b71";
$https = false;
$ptcHTTP = $https ? 'https' : 'http';

if ($server == $localhost) {
    define('HOME', $ptcHTTP."://".$server."/veiculos");
    define('HOST', "localhost");
    define('USER', "root");
    define('PASS', "");
    define('DBSA', "vehicle");
} else {
    define('HOME', "");
    define('HOST', "");
    define('USER', "");
    define('PASS', "");
    define('DBSA', "");
}

$dbsa = DBSA;

define('APP_BASE', "./routes/");
define('APP_LINK', "./public/");
define('APP_NAME', "Revenda do Capeta");
define('APP_DESC', "Desc da Revenda");
define('APP_URL', HOME);
define('SESSION_USER', $sessao);
define('CHAVE', $chave);
define('RESOURCES', "./resources/assets/");
define('FONTAWESOME', "https://use.fontawesome.com/releases/v5.4.1/css/all.css");

// defines de tabelas de banco de dados
define('CLIENTS', "$dbsa.clients");
define('SELLERS', "$dbsa.sellers");
define('USERS', "$dbsa.users");
define('VEHICLES', "$dbsa.vehicles");