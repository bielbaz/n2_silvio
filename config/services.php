<?php

class Service {
    private $db;
    private $lastid;

    public function __construct($db) {
        $this->db = $db->connect();
    }

    public function ExeRead($tabela, $cond = NULL) {
        $query = "SELECT * FROM {$tabela} {$cond}";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $retorno = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $retorno;
    }

    public function ExeCreate($tabela, array $datas) {
        $fields = implode(", ", array_keys($datas));
        $values = ":" . implode(", :", array_keys($datas));
        $query = "INSERT INTO {$tabela} ($fields) VALUES ($values)";
        $stmt = $this->db->prepare($query);

        foreach($datas as $key => $data):
            $stmt->bindValue(":$key",$datas[$key]);
        endforeach;

        $stmt->execute();
        $id = $this->db->lastInsertId();
        return $this->setLastId($id);
    }

    public function getLastId(){
        return $this->lastid;
    }

    public function setLastId($id){
        $this->lastid = $id;
    }

    public function ExeUpdate($tabela, array $datas, $where) {
        foreach ($datas as $fields => $values):
            $campos[] = "$fields = '$values'";
        endforeach;

        $campos = implode(", ", $campos);
        $query = "UPDATE {$tabela} SET $campos WHERE {$where}";
        $stmt = $this->db->prepare($query);
        $ret = $stmt->execute();
    }

    public function ExeDelete($tabela, $where) {
        $query = "DELETE FROM {$tabela} WHERE {$where}";
        $stmt = $this->db->prepare($query);
        $ret = $stmt->execute();

        if($ret){
            return true;
        } else {
            return false;
        }
    }

    public function Encrypt($frase, $chave, $crypt) {
        $retorno = "";
        
        if ($frase == '')
            return '';
        if ($crypt) {
            $string = $frase;
            $i = strlen($string) - 1;
            $j = strlen($chave);

            do {
                $retorno .= ($string{$i} ^ $chave{$i % $j});
            } while ($i--);

            $retorno = strrev($retorno);
            $retorno = base64_encode($retorno);
        } else {
            $string = base64_decode($frase);
            $i = strlen($string) - 1;
            $j = strlen($chave);

            do {
                $retorno .= ($string{$i} ^ $chave{$i % $j});
            } while ($i--);

            $retorno = strrev($retorno);
        }

        return $retorno;
    }

    /**
    * Função para gerar senhas aleatórias
    *
    * @author    Thiago Belem <contato@thiagobelem.net>
    *
    * @param integer $tamanho Tamanho da senha a ser gerada
    * @param boolean $maiusculas Se terá letras maiúsculas
    * @param boolean $numeros Se terá números
    * @param boolean $simbolos Se terá símbolos
    *
    * @return string A senha gerada
    */
    public function code($tamanho = 5, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;
        
        $len = strlen($caracteres);
        
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }

        return $retorno;
    }
}