CREATE DATABASE IF NOT EXISTS `vehicle`;

USE `vehicle`;

CREATE TABLE IF NOT EXISTS `clients`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `nascimento` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `sellers`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `nascimento` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `users`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ultimoId` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) NOT NULL,
  `modelo` varchar(255) NOT NULL,
  `ano` int(11) NOT NULL,
  `quilometragem` varchar(255) NOT NULL,
  `combustivel` int(11) NOT NULL,
  `portas` int(11) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `users` (`id`, `ultimoId`, `email`, `senha`, `tipo`) VALUES ('1', '0', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'A');
