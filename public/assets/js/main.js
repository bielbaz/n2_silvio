$(function () {
    var url = 'routes/tasks/';

    $('form[name="login"]').on('submit', function () {
        var task = "login";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'Para continuar com o login é necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    window.location.href = "index.php?page=home";
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('form[name="newClient"]').on('submit', function () {
        var task = "newClient";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro realizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('form[name="newSeller"]').on('submit', function () {
        var task = "newSeller";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro realizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('form[name="newCar"]').on('submit', function () {
        var task = "newCar";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro realizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('form[name="updateCar"]').on('submit', function () {
        var task = "updateCar";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro atualizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('form[name="updateSeller"]').on('submit', function () {
        var task = "updateSeller";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro atualizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });
    
    $('form[name="updateClient"]').on('submit', function () {
        var task = "updateClient";
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task },
            type: 'POST',
            success: function (data) {
                if (data == 'err_empty') {
                    UIkit.notification({
                        message: 'É necessário preencher todos os campos!',
                        status: 'primary',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Cadastro realizado com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });    

    $('a[href="#delCar"]').on('click', function () {
        var task = "delCar";
        var token = $(this).data('token');
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task, token: token },
            type: 'POST',
            success: function (data) {
                if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Exclusão realizada com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });

    $('a[href="#delSeller"]').on('click', function () {
        var task = "delSeller";
        var token = $(this).data('token');
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task, token: token },
            type: 'POST',
            success: function (data) {
                if (data == 'err_data') {
                    UIkit.notification({
                        message: 'Os dados inseridos estão inválidos, tente novamente!',
                        status: 'warning',
                        pos: 'top-center',
                        timeout: 5000
                    });
                } else if (data == 'success') {
                    UIkit.notification({
                        message: 'Exclusão realizada com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });
    
    $('a[href="#delClient"]').on('click', function () {
        var task = "delClient";
        var token = $(this).data('token');
        $(this).ajaxSubmit({
            url: url + 'main.php',
            data: { action: task, token: token },
            type: 'POST',
            success: function (data) {
                if (data == 'success') {
                    UIkit.notification({
                        message: 'Exclusão realizada com sucesso!',
                        status: 'success',
                        pos: 'top-center',
                        timeout: 3000
                    });
                    setInterval(function () { location.reload(); }, 3000);
                } else {
                    UIkit.notification({
                        message: 'Parece que ocorreu um erro, comunique um administrador!',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 5000
                    });
                }
            },
            beforeSend: function () { },
            complete: function () { },
            error: function () { alert("Error JS"); }
        });
        return false;
    });    
});