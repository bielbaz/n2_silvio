<?php
    $exe = new Service($conn);
    if(!empty($_SESSION[SESSION_USER])){
        $ssuser = $_SESSION[SESSION_USER];
        $readSSUser = $exe->ExeRead(USERS, "WHERE id='$ssuser'");
        if($readSSUser){
            foreach($readSSUser as $user);
        } else {
            header('Location: index.php?page=logout');
        }
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="<?= RESOURCES . "css/uikit.css"; ?>">
        <link rel="stylesheet" href="<?= APP_LINK . "assets/css/main.css"; ?>">
		
		<link rel="stylesheet" type="text/css" href="<?= APP_LINK . "assets/css/"; ?>source/bootstrap-3.3.6-dist/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?= APP_LINK . "assets/css/"; ?>source/font-awesome-4.5.0/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="<?= APP_LINK . "assets/css/"; ?>style/slider.css">
		<link rel="stylesheet" type="text/css" href="<?= APP_LINK . "assets/css/"; ?>style/mystyle.css">
		
        <?php
            $routes = filter_input(INPUT_GET, "page", FILTER_DEFAULT);
            switch($routes){
                case "login":
                    require_once APP_BASE . "login.php";
                break;
                case "logout":
                    require_once APP_BASE . "logout.php";
                break;
                case "new_client":
                    require_once APP_BASE . "new_client.php";
                break;
                case "new_seller":
                    require_once APP_BASE . "new_seller.php";
                break;
                case "new_car":
                    require_once APP_BASE . "new_car.php";
                break;
				case "inicial":
                    require_once APP_BASE . "inicial.php";
                break;
                default:
                    if(empty($_SESSION[SESSION_USER])){
                        require_once APP_BASE . "inicial.php";
                    } else {
						if($routes=="login"){
							require_once APP_BASE . "login.php";
						} elseif($routes=="home") {
							require_once APP_BASE . "home.php";
						} else {
                            require_once APP_BASE . "inicial.php";
                        }
                    }
                break;
            }
        ?>

        <script src="<?= RESOURCES . "js/jquery-2.2.4.min.js"; ?>"></script>
        <script src="<?= RESOURCES . "js/jquery.form.min.js"; ?>"></script>
        <script src="<?= RESOURCES . "js/jquery.mask.min.js"; ?>"></script>
        <script src="<?= RESOURCES . "js/uikit.min.js"; ?>"></script>
        <script src="<?= RESOURCES . "js/uikit-icons.min.js"; ?>"></script>
        <script src="<?= APP_LINK . "assets/js/main.js"; ?>"></script>
		
		<script type="text/javascript" src="<?= APP_LINK . "assets/js/"; ?>source/js/isotope.js"></script>
		<script type="text/javascript" src="<?= APP_LINK . "assets/js/"; ?>source/js/myscript.js"></script>
		<script type="text/javascript" src="<?= APP_LINK . "assets/js/"; ?>source/bootstrap-3.3.6-dist/js/jquery.1.11.js"></script>
		<script type="text/javascript" src="<?= APP_LINK . "assets/js/"; ?>source/bootstrap-3.3.6-dist/js/bootstrap.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".CPF").mask("000.000.000-00");
                $(".nascimento").mask("00/00/0000");
                $(".telefone").mask("(00) 00000-0000");
                $(".valor").mask("#,###,##0.00", {reverse: true});
                $(".quilometragem").mask("000.000", {reverse: true});
                $(".").mask("");
                $(".").mask("");
            })
        </script>
    </body>
</html>