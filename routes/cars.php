<div class="container" style="margin-top:50px;">
    <div style="float:left;background:#eee;width:100%;padding:15px;margin:15px 0;text-align:right;text-decoration:underline;">
        <a href="index.php?page=home&view=new_car">Novo Veiculo?</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="text-center">Marca</th>
                <th class="text-center">Modelo</th>
                <th class="text-center">Ano do Modelo</th>
                <th class="text-center">Quilometragem</th>
                <th class="text-center">Combustivel</th>
                <th class="text-center">Portas</th>
                <th class="text-center">Valor</th>
                <th class="text-center" colspan="2">A&ccedil;&otilde;es</th>
            </tr>
        </thead>
        <tbody >
            <?php
                $readVehicles = $exe->ExeRead(VEHICLES);
                if($readVehicles){
                    foreach($readVehicles as $v){
            ?>
            <tr>
                <td class="text-center"><?= $v['marca']; ?></td>
                <td class="text-center"><?= $v['modelo']; ?></td>
                <td class="text-center"><?= $v['ano']; ?></td>
                <td class="text-center"><?= $v['quilometragem']; ?></td>
            <?php if($v['combustivel']==1){ ?>
                <td class="text-center">Gasolina</td>
            <?php } elseif($v['combustivel']==2){ ?>
                <td class="text-center">Etanol</td>
            <?php } elseif($v['combustivel']==3){ ?>
                <td class="text-center">Diesel</td>
            <?php } elseif($v['combustivel']==4){ ?>
                <td class="text-center">Gás</td>
            <?php } else { ?>
                <td class="text-center">Gasolina + Gás</td>
            <?php } ?>
                <td class="text-center"><?= $v['portas']; ?></td>
                <td class="text-center"><?= $v['valor']; ?></td>
                <td class="text-center" cowspan="2">
                    <a href='index.php?page=home&view=editCar&id=<?= $v['id']; ?>' class='btn btn-default btn-xs'>
                        <span class='glyphicon glyphicon-pencil'></span>Editar
                    </a>
                </td>
                <td class="text-center">
                    <a href='#delCar' data-token='<?= $v['id']; ?>' class='btn btn-danger btn-xs'>
                        <span class='glyphicon glyphicon-remove'></span>Excluir
                    </a>
                </td>
            </tr>
            <?php
                    }
                }
            ?>
        </tbody>
    </table>
</div>