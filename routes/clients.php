<div class="container" style="margin-top:50px;">
    <div style="float:left;background:#eee;width:100%;padding:15px;margin:15px 0;text-align:right;text-decoration:underline;">
        <a href="index.php?page=home&view=new_client">Novo Cliente?</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="text-center">Nome</th>
                <th class="text-center">CPF</th>
                <th class="text-center">Nascimento</th>
                <th class="text-center">Telefone</th>
                <th class="text-center">Endere&ccedil;o</th>
                <th class="text-center" colspan="2">A&ccedil;&otilde;es</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $readClients = $exe->ExeRead(CLIENTS, "ORDER BY id DESC");
                if($readClients){
                    foreach($readClients as $c){
            ?>
            <tr>
                <td class="text-center"><?=$c['nome'];?></td>
                <td class="text-center"><?=$c['cpf'];?></td>
                <td class="text-center"><?=$c['nascimento'];?></td>
                <td class="text-center"><?=$c['telefone'];?></td>
                <td class="text-center"><?=$c['endereco'];?></td>
                <td class="text-center" cowspan="2">
                    <a href='index.php?page=home&view=editClient&id=<?= $c['id']; ?>' class='btn btn-default btn-xs'>
                        <span class='glyphicon glyphicon-pencil'></span>Editar
                    </a>
                </td>
                <td class="text-center">
                    <a href='#delClient' data-token='<?= $c['id']; ?>' class='btn btn-danger btn-xs'>
                        <span class='glyphicon glyphicon-remove'></span>Excluir
                    </a>
                </td>
            </tr>
            <?php    
                    }

                }
            ?>
        </tbody>
    </table>
</div>