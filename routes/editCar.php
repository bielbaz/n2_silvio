<div class="container" style="margin-top:50px;">
    <?php
        $carId = filter_input(INPUT_GET, "id", FILTER_DEFAULT);
        if(!empty($carId)){
            $readEditCar = $exe->ExeRead(VEHICLES, "WHERE id='$carId'");
            if($readEditCar){
                foreach($readEditCar as $editCar);
    ?>
    <form name="updateCar" method="POST" class="form">
        <table>
            <tr>
                <td>Marca: </td> <td><input type="text" name="marca" value="<?= $editCar['marca']; ?>" required></td>
            </tr>
            <tr>
                <td>Modelo: </td> <td><input type="text" name="modelo" value="<?= $editCar['modelo']; ?>" required></td>
            </tr>
            <tr>
                <td>Ano: </td>
                <td>
                    <select required name="ano">
                        <?php 
                            for ($i=2019; $i >=1910 ; $i--) { 
                                if($editCar['ano']==$i){
                                    echo '<option value = "'.$i.'" selected>'.$i.'</option>';
                                } else {
                                    echo '<option value = "'.$i.'">'.$i.'</option>';
                                }
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Quilometragem:</td> <td><input type="text" name="quilometragem" class="quilometragem" value="<?= $editCar['quilometragem']; ?>" required></td>
            </tr>
            <tr>
                <td>Combustivel: </td>
                <td>
                    <select name="combustivel">
                        <option value="1" <?php if($editCar['combustivel']==1){ echo "selected"; } ?>>Gasolina</option>
                        <option value="2" <?php if($editCar['combustivel']==2){ echo "selected"; } ?>>Etanol</option>
                        <option value="3" <?php if($editCar['combustivel']==3){ echo "selected"; } ?>>Diesel</option>
                        <option value="4" <?php if($editCar['combustivel']==4){ echo "selected"; } ?>>Gás</option>
                        <option value="5" <?php if($editCar['combustivel']==5){ echo "selected"; } ?>>Gasolina + Gás</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Portas: </td>
                <td>
                    <select name="portas">
                        <option value="1" <?php if($editCar['portas']==1){ echo "selected"; } ?>>Sem portas</option>
                        <option value="2" <?php if($editCar['portas']==2){ echo "selected"; } ?>>2</option>
                        <option value="3" <?php if($editCar['portas']==3){ echo "selected"; } ?>>3</option>
                        <option value="4" <?php if($editCar['portas']==4){ echo "selected"; } ?>>4</option>
                        <option value="5" <?php if($editCar['portas']==5){ echo "selected"; } ?>>5 ou mais</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Foto:</td>
                <td><input type="file" name="imagem"></td>
            </tr>
            <tr>
                <td>Valor:</td>
                <td><input type="text" name="valor" value="<?= $editCar['valor']; ?>"></td>
            </tr>
            <input type="hidden" value="<?= $exe->Encrypt($editCar['id'], CHAVE, true); ?>" name="token">
            <tr><td></td> <td><button type="submit">Atualizar</button> <a href="index.php?page=home&view=cars">Cancelar</button></td></tr>
        </table>
    </form>
    <?php
            }
        }
    ?>
</div>