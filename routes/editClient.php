<div class="container" style="margin-top:50px;">
    <?php
        $clientId = filter_input(INPUT_GET, "id", FILTER_DEFAULT);
        if(!empty($clientId)){
            $readEditClient = $exe->ExeRead(CLIENTS, "WHERE id='$clientId'");
            if($readEditClient){
                foreach($readEditClient as $editClient){
    ?>
    <form name="updateClient" method="post">
        <table>
            <tr> <td>Nome do cliente: </td> <td><input type="text" name="nome" value="<?= $editClient['nome']; ?>" placeholder="Nome e sobrenome" required></td> </tr>
            <tr> <td>CPF do cliente: </td> <td><input type="text" name="cpf" value="<?= $editClient['cpf']; ?>" class="CPF" placeholder="000.000.000-00" required></td> </tr>
            <tr> <td>Data de nascimento do cliente: </td> <td><input type="text" name="nascimento" value="<?=$editClient['nascimento']; ?>" class="nascimento" placeholder="Data de Nascimento" required></td> </tr>
            <tr> <td>Telefone do cliente: </td> <td><input type="text" name="telefone" value="<?= $editClient['telefone']; ?>" class="telefone" placeholder="(DD) X XXXX-XXXX" required></td> </tr>
            <tr> <td>Endereço do cliente: </td> <td><input type="text" name="endereco" value="<?= $editClient['endereco']; ?>" placeholder="Rua Exemplo, XXX, Cidade X" required></td> </tr>
            <input type="hidden" value="<?= $exe->Encrypt($c['id'], CHAVE, true); ?>" name="token">
            <tr><td></td> <td><button type="submit">Atualizar</button> <a href="index.php?page=home&view=clients">Cancelar</button></td></tr>
        </table>
    </form>                    
    <?php                    
                }
            }
        }
    ?>
</div>