<div class="container" style="margin-top:50px;">
    <?php
        $sellerId = filter_input(INPUT_GET, "id", FILTER_DEFAULT);
        if(!empty($sellerId)){
            $readEditSeller = $exe->ExeRead(SELLERS, "WHERE id='$sellerId'");
            if($readEditSeller){
                foreach($readEditSeller as $editSeller){
    ?>
    <form name="updateSeller" method="post" class="form">
        <table>
            <tr> <td>Nome do vendedor: </td> <td><input type="text" name="nome" value="<?= $editSeller['nome']; ?>" placeholder="Nome e sobrenome" required></td> </tr>
            <tr> <td>CPF do vendedor: </td> <td><input type="text" name="cpf" value="<?= $editSeller['cpf']; ?>" class="CPF" placeholder="000.000.000-00" required></td> </tr>
            <tr> <td>Data de nascimento do vendedor: </td> <td><input type="text" name="nascimento" value="<?= $editSeller['nascimento']; ?>" class="nascimento" placeholder="Data de Nascimento" required></td> </tr>
            <tr> <td>Telefone do vendedor: </td> <td><input type="text" name="telefone" value="<?= $editSeller['telefone']; ?>" class="telefone" placeholder="(DD) X XXXX-XXXX" required></td> </tr>
            <tr> <td>Endereço do vendedor: </td> <td><input type="text" name="endereco" value="<?= $editSeller['endereco']; ?>" placeholder="Rua Exemplo, XXX, Cidade X" required></td> </tr>
            <input type="hidden" value="<?= $exe->Encrypt($editSeller['id'], CHAVE, true); ?>" name="token">
            <tr><td></td> <td><button type="submit">Atualizar</button> <a href="index.php?page=home&view=sellers">Cancelar</button></td></tr>
        </table>
    </form>
    <?php
                }
            }
        }
    ?>
</div>