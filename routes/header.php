<div class="header">
    <ul class="socialicon">
        <li><a href="https://www.facebook.com/escolhaQI/"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://www.instagram.com/escolhaqi/"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://twitter.com/escolhaqi"><i class="fa fa-twitter"></i></a></li>
        <li><a href="https://www.linkedin.com/company/qi-escolas-e-faculdades"><i class="fa fa-linkedin"></i></a></li>
    </ul>
    <ul class="givusacall">
        <li>Ligue para nós : 0800 601 0000</li>
    </ul>
    <ul class="logreg">
        <li><a href="index.php?page=logout">Desconectar</a></li>
    </ul>
</div>
<div class="container">
    <div class="navbar-header">
        <a class="navbar-brand logo" href="index.php?page=home"><img src="<?= APP_LINK; ?>assets/images/logo1.png" alt="logo"></a>
    </div>	 
</div>
<?php if($user['tipo']=="A") { ?>
<nav class="topnavbar navbar-default topnav">
    <div class="collapse navbar-collapse" id="upmenu">
        <ul class="nav navbar-nav" id="navbarontop">
            <li><a href="index.php?page=home">Dashboard</a></li>
            <li><a href="index.php?page=home&view=clients">Clientes</a></li>
            <li><a href="index.php?page=home&view=cars">Veiculos</a></li>
            <button><span class="postnewcar"><a href="index.php?page=home&view=sellers">Vendedores</a></span></button>
        </ul>
    </div>
</nav>
<?php } elseif($user['tipo']=="S") { ?>
<nav class="topnavbar navbar-default topnav">
    <div class="collapse navbar-collapse" id="upmenu">
        <ul class="nav navbar-nav" id="navbarontop">
            <li><a href="index.php?page=home">Dashboard</a></li>
            <li><a href="index.php?page=home&view=cars">Veiculos</a></li>
            <button><span class="postnewcar"><a href="index.php?page=home&view=clients">Clientes</a></span></button>
        </ul>
    </div>
</nav>
<?php } else { ?>
<nav class="topnavbar navbar-default topnav">
    <div class="collapse navbar-collapse" id="upmenu">
        <ul class="nav navbar-nav" id="navbarontop">
            <li><a href="index.php?page=home">Dashboard</a></li>
            <button><span class="postnewcar"><a href="index.php?page=home&view=cars">Veiculos</a></span></button>
        </ul>
    </div>
</nav>
<?php } ?>