        <?php
            if(empty($_SESSION[SESSION_USER])){
                header('Location: index.php?page=logout');
            }
        ?>
        <title><?= APP_NAME; ?></title>
    </head>
    <body class="allcontain">
        <?php
            require_once APP_BASE . "header.php";
            $view = filter_input(INPUT_GET, "view", FILTER_DEFAULT);
            unset($_GET['view']);
            if(!empty($view)){
                switch($view){
                    case "new_client":
                        require_once APP_BASE . "new_client.php";
                    break;
                    case "new_car":
                        require_once APP_BASE . "new_car.php";
                    break;
                    case "new_seller":
                        require_once APP_BASE . "new_seller.php";
                    break;
                    case "clients":
                        require_once APP_BASE . "clients.php";
                    break;
                    case "cars":
                        require_once APP_BASE . "cars.php";
                    break;
                    case "sellers":
                        require_once APP_BASE . "sellers.php";
                    break;
                    case "editClient":
                        require_once APP_BASE . "editClient.php";
                    break;
                    case "editSeller":
                        require_once APP_BASE . "editSeller.php";
                    break;
                    case "editCar":
                        require_once APP_BASE . "editCar.php";
                    break;
                    default:
                        echo "Something is wrong, ma' boy...";
                    break;
                }
            }