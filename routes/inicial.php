<title><?= APP_NAME; ?></title>
    </head>
    <body>
        <!-- Header -->
        <div class="allcontain">
            <div class="header">
                    <ul class="socialicon">
                        <li><a href="https://www.facebook.com/escolhaQI/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/escolhaqi/"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://twitter.com/escolhaqi"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/qi-escolas-e-faculdades"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                    <ul class="givusacall">
                        <li>Ligue para nós : 0800 601 0000 </li>
                    </ul>
            </div>
            <!-- Navbar Up -->
            <nav class="topnavbar navbar-default topnav">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand logo" href="#"><img src="<?= APP_LINK; ?>assets/images/logo1.png" alt="logo"></a>
                    </div>	 
                </div>
                <div class="collapse navbar-collapse" id="upmenu">
                    <ul class="nav navbar-nav" id="navbarontop">
                        <?php
                            if(empty($_SESSION[SESSION_USER])){
                                echo "<button style= 'position: relative; left: 80%;'>".
                                        "<span class='postnewcar'>".
                                            "<a href='index.php?page=login'>Login</a>".
                                        "</span>".
                                    "</button>";
                            } else {
                                echo "<li><a href='index.php?page=home'>Painel</a></li>";
                            }
                        ?>
                    </ul>
                </div>
            </nav>
        </div>
        <!--_______________________________________ Carousel__________________________________ -->
        <div class="allcontain">
            <div id="carousel-up" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner " role="listbox">
                    <div class="item active">
                        <img src="<?= APP_LINK; ?>assets/images/oldcar.jpg" alt="oldcar">
                        <div class="carousel-caption">
                            <h2>Porsche 356</h2>
                            <p>É uma linha de automóveis produzidos de 1948 até 1965,<br>
                            e é considerado o primeiro carro produzido pela Porsche.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?= APP_LINK; ?>assets/images/porche.jpg" alt="porche">
                        <div class="carousel-caption">
                            <h2>Porsche 550 Spyder</h2>
                                <p>Inspirada pelos pequenos modelos Spyder que surgiam no cenário das corridas da época<br/> 
                                (particularmente por um pequeno Porsche 356 Spyder desenvolvido por Walter Glöckler em 1951),<br/>
                                a fábrica decidiu construir um carro semelhante, seu primeiro projeto dedicado especificamente a competições.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ____________________Featured Section ______________________________--> 
        <div class="allcontain">
            <div class="feturedsection">
                <h1 class="text-center"><span class="bdots">&bullet;</span>CARACTERÍSTICAS<span class="carstxt">CARROS</span>&bullet;</h1>
            </div>
            <div class="feturedimage">
                <div class="row firstrow">
                    <div class="col-lg-6 costumcol colborder1">
                        <div class="row costumrow">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 img1colon">
                                <img src="<?= APP_LINK; ?>assets/images/featurporch.jpg" alt="porsche">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 txt1colon ">
                                <div class="featurecontant">
                                    <h1>Porsche 550 Spyder</h1>
                                    <p>"O Porsche 550 foi um carro desportivo produzido pela Porsche durante os anos 1950." </p>
                                    <h2>R&#36; 46,900</h2>
                                    <button id="btnRM" onClick="rmtxt()">Saiba mais</button>
                                     
 

                                    <div id="readmore">
                                            <h1>Porsche 550 Spyder</h1>
                                            <p>"Inspirada pelos pequenos modelos Spyder que
                                            surgiam no cenário das corridas da época (particularmente por um pequeno 
                                            Porsche 356 Spyder desenvolvido por Walter Glöckler em 1951), a fábrica decidiu construir um carro semelhante,
                                            seu primeiro projeto dedicado especificamente a competições. O 550 ficou conhecido como Spyder ou RS, 
                                            e deu a Porsche sua primeira vitória geral em uma cometição de grande porte, a Targa Florio de 1956.</P>
                                            <button id="btnRL">LEIA MENOS</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 costumcol colborder2">
                        <div class="row costumrow">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 img2colon">
                                <img src="<?= APP_LINK; ?>assets/images/car3.jpg" alt="porsche1">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 txt1colon ">
                                <div class="featurecontant">
                                    <h1>Ford Mustang 69'</h1>
                                    <p>O nome "Mustang" é inspirado no avião de caça estadunidense P-51 Mustang, 
                                            cujo nome se inspira na unica raça de cavalo selvagem do pais. 
                                            Foi o primeiro "Muscle Car" da história, 
                                            sendo seguido anos mais tarde por modelos concorrentes inspirados claramente nele.</p>
                                    <h2>R&#36; 30,000</h2>
                                    <button id="btnRM2">Saiba mais</button>
                                    <div id="readmore2">
                                            <h1>Ford Mustang 69'</h1>
                                            <p>Este modelo é de uma nova atualização de estilo, tornado a linha mais atual e agressiva para a época. 
                                            Lançamento dos modelos BOSS 302 e 429, equipados com motores de alto desempenho e das linhas Mach 1(Esportiva) e Grande (Luxuosa).</P>
                                            <button id="btnRL2">LEIA MENOS</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- ________________________LATEST CARS SECTION _______________________-->
        <div class="latestcars">
            <h1 class="text-center">&bullet; ÚLTIMOS CARROS &bullet;</h1>
        </div>
        <!-- ________________________Latest Cars Image Thumbnail________________-->
        <div class="grid">
            <div class="row">
                <?php
                    $readLastCars = $exe->ExeRead(VEHICLES, "ORDER BY id DESC");
                    if($readLastCars){
                        foreach($readLastCars as $lastCar){
                ?>
                <div class="lastcar col-xs-12 col-sm-6 col-md-4 col-lg-3" style="background: url(<?= APP_LINK . "uploads/" . $lastCar['imagem']; ?>) center center no-repeat;background-size:auto 100%;">
                    <div class="txthover">
                        <div class="txtcontent">
                            <div class="stars">
                                <div class="glyphicon glyphicon-star"></div>
                                <div class="glyphicon glyphicon-star"></div>
                                <div class="glyphicon glyphicon-star"></div>
                            </div>
                            <div class="simpletxt">
                                <h3 class="name"><?= $lastCar['marca']." ".$lastCar['modelo']; ?></h3>
                                <h4 class="price"> R&#36; <?= $lastCar['valor']; ?></h4>
                                <button>Saiba mais</button><br>
                            </div>
                            <div class="stars2">
                                <div class="glyphicon glyphicon-star"></div>
                                <div class="glyphicon glyphicon-star"></div>
                                <div class="glyphicon glyphicon-star"></div>
                            </div>
                        </div>
                    </div>	 
                </div>
                <?php
                        }
                    }
                ?>
            </div>
        </div>
        <!-- ACABA AQUI ESSA LOLI LINDA <3 -->

            <!-- ______________________________________________________Bottom Menu ______________________________-->
            <div class="bottommenu" > 
                <div class="bottomlogo">
                <span class="dotlogo">&bullet;</span><img src="<?= APP_LINK; ?>assets/images/collectionlogo1.png" alt="logo1"><span class="dotlogo">&bullet;;</span>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d726.1979747483983!2d-51.222382843905926!3d-30.025403389222994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95197909931aaaab%3A0x9ea0b9dcf7a4a29b!2sQI+Faculdade+%26+Escola+T%C3%A9cnica+%7C+Julio+de+Castilhos+%7C+Porto+Alegre!5e0!3m2!1spt-BR!2sbr!4v1545136562872" width="1366" height="559" frameborder="0" style="border:0" allowfullscreen></iframe>
                
                <div class="bottomsocial">
                    <a href="https://www.facebook.com/escolhaQI/"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/escolhaqi/"><i class="fa fa-instagram"></i></a>
                    <a href="https://twitter.com/escolhaqi"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.linkedin.com/company/qi-escolas-e-faculdades"><i class="fa fa-linkedin"></i></a>
                </div>
                    <div class="footer">
                        <div class="copyright">	
                        &copy; Copyright 2018 | <a href="#">Privacy </a>| <a href="#">Policy</a>
                        </div>
                        <div class="atisda">
                            Designed by <a href="http://www.webdomus.net/">Web Domus Italia - Web Agency </a> 
                        </div>
                    </div>
            </div>
        </div>