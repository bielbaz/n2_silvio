        <?php
            if(!empty($_SESSION[SESSION_USER])){
                header('Location: index.php');
            }
        ?>
        <title>Acesso Restrito</title>
    </head>
    <body>
        <div class="wrapper">
            <form class="form-signin" name="login" method="post">       
                <h2 class="form-signin-heading">Acesso Restrito</h2>
                <input type="email" class="form-control" name="email" placeholder="Email" required="" autofocus="">
                <input type="password" class="form-control" name="senha" placeholder="Senha" required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                <center><a href="index.php?page=inicial">Voltar para Página Inicial</a></center>
            </form>
        </div>