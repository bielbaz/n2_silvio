<div class="container" style="margin-top:50px;">
    <form name="newCar" method="POST">
        <table>
            <tr>
                <td>Marca: </td> <td><input type="text" name="marca" required></td>
            </tr>
            <tr>
                <td>Modelo: </td> <td><input type="text" name="modelo" required></td>
            </tr>
            <tr>
                <td>Ano: </td>
                <td>
                    <select required name="ano">
                        <?php 
                            for ($i=2019; $i >=1910 ; $i--) { 
                                echo '<option value = "'.$i.'">'.$i.'</option>';
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Quilometragem:</td> <td><input type="text" name="quilometragem" class="quilometragem"required></td>
            </tr>
            <tr>
                <td>Combustivel: </td>
                <td>
                    <select name="combustivel">
                        <option value="1">Gasolina</option>
                        <option value="2">Etanol</option>
                        <option value="3">Diesel</option>
                        <option value="4">Gás</option>
                        <option value="5">Gasolina + Gás</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Portas: </td>
                <td>
                    <select name="portas">
                        <option value="1">Sem portas</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5 ou mais</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Foto:</td>
                <td><input type="file" name="imagem"></td>
            </tr>
            <tr>
                <td>Valor:</td>
                <td><input type="text" name="valor" class="valor"></td>
            </tr>
            <tr><td></td> <td><button type="submit">Cadastrar</button> <a href="index.php?page=home&view=cars">Cancelar</button></td></tr>
        </table>
    </form>
</div>