<div class="container" style="margin-top:50px;">
	<form name="newSeller" method="post">
		<b>Todos os campos são obrigatórios!</b>
		<table>
			<tr>
				<td>Nome do vendedor: </td> <td><input type="text" name="nome" placeholder="Nome e sobrenome" required></td>
			</tr>
			<tr>
				<td>CPF do vendedor: </td> <td><input type="text" name="cpf" class="CPF" placeholder="000.000.000-00" required></td>
			</tr>
			<tr>
				<td>Data de nascimento do vendedor: </td> <td><input type="text" name="nascimento" class="nascimento" required></td>
			</tr>
			<tr>
				<td>Telefone do vendedor: </td> <td><input type="text" name="telefone" class="telefone" placeholder="(DD) X XXXX-XXXX" required></td>
			</tr>
			<tr>
				<td>Endereço do vendedor: </td> <td><input type="text" name="endereco" placeholder="Rua Exemplo, XXX, Cidade X" required></td>
			</tr>
			<tr>
				<td>Email para login do vendedor: </td> <td><input type="email" name="email" placeholder="exemplo@exemplo.com.br" required></td>
			</tr>
			<tr>
				<td>Senha para login do vendedor: </td> <td><input type="password" name="senha" placeholder="Digite sua senha" required></td>
			</tr>
			<input type="hidden" name="tipo" value="S">
			<tr><td></td> <td><button type="submit">Cadastrar</button> <a href="index.php?page=home&view=sellers">Cancelar</button></td></tr>
		</table>
	</form>
</div>