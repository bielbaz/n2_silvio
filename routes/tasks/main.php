<?php
ob_start();
session_start();
header('Content-Type: text/html; charset=utf-8');
require_once('../../config/app.php');
$exe = new Service($conn);
$action = filter_input(INPUT_POST, "action", FILTER_DEFAULT);
unset($_POST['action']);
switch($action){
    case "login":
        $email = filter_input(INPUT_POST, "email", FILTER_DEFAULT);
        $senha = md5(filter_input(INPUT_POST, "senha", FILTER_DEFAULT));
        unset($_POST['email'], $_POST['senha']);
        if(!$email || !$senha){
            echo "err_empty";
        } else {
            $readUserLogin = $exe->ExeRead(USERS, "WHERE email='$email' AND senha='$senha'");
            if(!$readUserLogin){
                echo "err_data";
            } else {
                foreach($readUserLogin as $user);
                $_SESSION[SESSION_USER] = $user['id'];
                echo "success";
            }
        }
        break;

    /* CREATES */
    case "newClient":
        $f['nome'] = filter_input(INPUT_POST, "nome", FILTER_DEFAULT);
        $f['cpf'] = filter_input(INPUT_POST, "cpf", FILTER_DEFAULT);
        $f['nascimento'] = filter_input(INPUT_POST, "nascimento", FILTER_DEFAULT);
        $f['telefone'] = filter_input(INPUT_POST, "telefone", FILTER_DEFAULT);
        $f['endereco'] = filter_input(INPUT_POST, "endereco", FILTER_DEFAULT);
        unset($_POST['nome'], $_POST['cpf'], $_POST['nascimento'], $_POST['telefone'], $_POST['endereco']);

        $exe->ExeCreate(CLIENTS, $f);

        $b = $exe->getLastId();
        
        $a['ultimoId'] = $b;
        $a['email'] = filter_input(INPUT_POST, "email", FILTER_DEFAULT);
        $a['senha'] = md5(filter_input(INPUT_POST, "senha", FILTER_DEFAULT));
        $a['tipo'] = filter_input(INPUT_POST, "tipo", FILTER_DEFAULT);
        unset($_POST['email'], $_POST['senha'], $_POST['tipo']);

        $exe->ExeCreate(USERS, $a);

        echo "success";
        
    break;

    case "newSeller":
        $f['nome'] = filter_input(INPUT_POST, "nome", FILTER_DEFAULT);
        $f['cpf'] = filter_input(INPUT_POST, "cpf", FILTER_DEFAULT);
        $f['nascimento'] = filter_input(INPUT_POST, "nascimento", FILTER_DEFAULT);
        $f['telefone'] = filter_input(INPUT_POST, "telefone", FILTER_DEFAULT);
        $f['endereco'] = filter_input(INPUT_POST, "endereco", FILTER_DEFAULT);
        unset($_POST['nome'], $_POST['cpf'], $_POST['nascimento'], $_POST['telefone'], $_POST['endereco']);

        $exe->ExeCreate(SELLERS, $f);

        $b = $exe->getLastId();
            
        $a['ultimoId'] = $b;
        $a['email'] = filter_input(INPUT_POST, "email", FILTER_DEFAULT);
        $a['senha'] = md5(filter_input(INPUT_POST, "senha", FILTER_DEFAULT));
        $a['tipo'] = filter_input(INPUT_POST, "tipo", FILTER_DEFAULT);
        unset($_POST['email'], $_POST['senha'], $_POST['tipo']);

        $exe->ExeCreate(USERS, $a);

        echo "success";
    break;

    case "newCar":
    $uploaddir = '../../public/uploads/';
    $uploadfile = $uploaddir . basename($_FILES['imagem']['name']);
    $newName = $exe->code(15);

	$f['marca'] = filter_input(INPUT_POST, "marca", FILTER_DEFAULT);
    $f['modelo'] = filter_input(INPUT_POST, "modelo", FILTER_DEFAULT);
    $f['ano'] = filter_input(INPUT_POST, "ano", FILTER_DEFAULT);
    $f['quilometragem'] = filter_input(INPUT_POST, "quilometragem", FILTER_DEFAULT);
    $f['combustivel'] = filter_input(INPUT_POST, "combustivel", FILTER_DEFAULT);
    $f['portas'] = filter_input(INPUT_POST, "portas", FILTER_DEFAULT);
    $f['imagem'] = $newName.basename($_FILES['imagem']['name']);
    $f['valor'] = filter_input(INPUT_POST, "valor", FILTER_DEFAULT);

    if (move_uploaded_file($_FILES['imagem']['tmp_name'], $uploaddir . $f['imagem'])) {
        echo "Arquivo válido e enviado com sucesso.\n";
    } else {
        echo "Possível ataque de upload de arquivo!\n";
    }

    unset($_POST['marca'], $_POST['modelo'], $_POST['ano'], $_POST['quilometragem'], $_POST['combustivel'], $_POST['portas'], $_FILES['imagem'], $_POST['valor']);

    $exe->ExeCreate(VEHICLES, $f);

    echo "success";

    break;

    /* UPDATES */
    case "updateCar":
        $carId = $exe->Encrypt(filter_input(INPUT_POST, "token", FILTER_DEFAULT), CHAVE, false);
        $f['marca'] = filter_input(INPUT_POST, "marca", FILTER_DEFAULT);
        $f['modelo'] = filter_input(INPUT_POST, "modelo", FILTER_DEFAULT);
        $f['ano'] = filter_input(INPUT_POST, "ano", FILTER_DEFAULT);
        $f['quilometragem'] = filter_input(INPUT_POST, "quilometragem", FILTER_DEFAULT);
        $f['combustivel'] = filter_input(INPUT_POST, "combustivel", FILTER_DEFAULT);
        $f['portas'] = filter_input(INPUT_POST, "portas", FILTER_DEFAULT);
        $f['valor'] = filter_input(INPUT_POST, "valor", FILTER_DEFAULT);
        unset($_POST['marca'], $_POST['modelo'], $_POST['ano'], $_POST['quilometragem'], $_POST['combustivel'], $_POST['portas'], $_POST['valor']);

        $exe->ExeUpdate(VEHICLES, $f, "id='$carId'");

        echo "success";
    break;

    case "updateSeller": 
        $sellerId = $exe->Encrypt(filter_input(INPUT_POST, "token", FILTER_DEFAULT), CHAVE, false);
        $f['nome'] = filter_input(INPUT_POST, "nome", FILTER_DEFAULT);
        $f['cpf'] = filter_input(INPUT_POST, "cpf", FILTER_DEFAULT);
        $f['nascimento'] = filter_input(INPUT_POST, "nascimento", FILTER_DEFAULT);
        $f['telefone'] = filter_input(INPUT_POST, "telefone", FILTER_DEFAULT);
        $f['endereco'] = filter_input(INPUT_POST, "endereco", FILTER_DEFAULT);
        unset($_POST['nome'], $_POST['cpf'], $_POST['nascimento'], $_POST['telefone'], $_POST['endereco']);

        $exe->ExeUpdate(SELLERS, $f, "id='$sellerId'");
        echo "success";
    break;

    case "updateClient": 
        $clientId = $exe->Encrypt(filter_input(INPUT_POST, "token", FILTER_DEFAULT), CHAVE, false);
        $f['nome'] = filter_input(INPUT_POST, "nome", FILTER_DEFAULT);
        $f['cpf'] = filter_input(INPUT_POST, "cpf", FILTER_DEFAULT);
        $f['nascimento'] = filter_input(INPUT_POST, "nascimento", FILTER_DEFAULT);
        $f['telefone'] = filter_input(INPUT_POST, "telefone", FILTER_DEFAULT);
        $f['endereco'] = filter_input(INPUT_POST, "endereco", FILTER_DEFAULT);
        unset($_POST['nome'], $_POST['cpf'], $_POST['nascimento'], $_POST['telefone'], $_POST['endereco']);

        $exe->ExeUpdate(CLIENTS, $f, "id='$clientId'");
        echo "success";
    break;

    /* DELETES */
    case "delCar":
        $token = filter_input(INPUT_POST, "token", FILTER_DEFAULT);
        unset($_POST['token']);
        $exe->ExeDelete(VEHICLES, "id='$token'");
        echo "success";
        break;
    case "delSeller":
        $token = filter_input(INPUT_POST, "token", FILTER_DEFAULT);
        unset($_POST['token']);
        $exe->ExeDelete(SELLERS, "id='$token'");
        echo "success";
        break;
    case "delClient":
        $token = filter_input(INPUT_POST, "token", FILTER_DEFAULT);
        unset($_POST['token']);
        $exe->ExeDelete(CLIENTS, "id='$token'");
        echo "success";
        break;
    default:
        echo "erro grave!";
        break;
}
ob_end_flush();